#!/usr/bin/env bash
#Script by hunch#2346

##FILL IN YOUR DETAILS HERE
EMAIL="@gmail.com" # your email for HGO
KEY="" #Your key for HGO
CUSTOM_DATA_URL="" #Fill in only if you need
CUSTOM_FETCH_URL="" #Fill in only if you need
UIV="True"
ULTRAQUEST="True"
FASTLOAD="True"
HIDEGYMS="True"
STARTLAT="2.976479"
STARTLNG="-1.253901"
OUTPUT_IPA_PATH="/var/local/Folder/Payload-notsigned.ipa" #Fill in if you want custom IPA name, otherwise the script will output to niantic.ipa


##DON'T EDIT BELOW THIS LINE

#Check for key
if [ -z "$KEY" ]
then
	echo "Please fill in your key"
	exit 1
fi

#check for ipa input and do our thing
if [ -z "$1" ]
then
	echo "Usage: ./ipa.sh path_to_ipa_file";
else
	echo "Unzipping ipa file"
	unzip -o -q $1
	echo "Unzipped"
	
	echo "Modifying plist file"
	/usr/libexec/PlistBuddy -c "set :Key $KEY" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :Email $EMAIL" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :UIV $UIV" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :UltraQuest $ULTRAQUEST" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :FastLoad $FASTLOAD" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :HideGyms $HIDEGYMS" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :StartLat $STARTLAT" "Payload/pokemongo.app/Mapper.plist"
        /usr/libexec/PlistBuddy -c "set :StartLng $STARTLNG" "Payload/pokemongo.app/Mapper.plist"
	if [ ! -z "$CUSTOM_DATA_URL" ]
	then
		/usr/libexec/PlistBuddy -c "set :DataURL1: $CUSTOM_DATA_URL" "Payload/pokemongo.app/Mapper.plist"
	fi
	if [ ! -z "$CUSTOM_LOC_URL" ]
	then
		/usr/libexec/PlistBuddy -c "set :FetchURL $CUSTOM_FETCH_URL" "Payload/pokemongo.app/Mapper.plist"
	fi
	echo "Modified"
	
	echo "Making new ipa file"
	IPA_PATH=""
	if [ -z "$OUTPUT_IPA_PATH" ]
	then
		IPA_PATH="niantic.ipa"
	else
		IPA_PATH=$OUTPUT_IPA_PATH
	fi
	zip -q -r $IPA_PATH Payload
	rm -rf Payload
	echo "Made $IPA_PATH"
fi

